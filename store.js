const fs = require('fs');
const path = require('path');


const store = () => {

    const dataFile = path.join(__dirname, 'dictionary.json');

    if (fs.existsSync(dataFile)) {

        let data = JSON.parse(fs.readFileSync(dataFile));
        const operation = process.argv[2];
        let key = process.argv[3];
        let value = process.argv[4] ? process.argv[4] : '';

        switch (operation) {
            case "add":
                if (key) {
                    data[key] = value;
                } else {
                    console.log('Invalid inputs, Please Enter Key');
                }
                break;

            case "list":
                console.log(data);
                break;
            case "get":
                if (key) {
                    console.log(`${ data[key] ? data[key] : 'No Value Found !' }`);
                } else {
                    console.log('Invalid Inputs , Please Enter Key');
                }
                break;

            case "remove":
                if (key) {
                    delete data[key];
                } else {
                    console.log('Invalid Inputs , Please Enter Key');
                }
                break;

            case "clear":
                data = {};
                break;

            default:
                console.log(`Invalid inputs , please type  ' node store.js list '`);

        }

        fs.writeFileSync(dataFile, JSON.stringify(data));
    } else {
        console.log('File Not Found');
    }


}


module.exports = store();